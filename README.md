# Plugin TokenCash

El Plugin integra la tienda electrónica con el mejor servicio de recompensas en Mejico, llamada **TokenCash** para poder premiar a sus clientes con cupones de descuento para usarlo al momento de la compra.

El plugin es realmente sencillo de usar. Genera un código QR que luego, puede ser escaneado desde la App de TokenCash de manera sencilla. Al momento de la compra, el Plugin intenta cada 70 segundos (configurable) verificar contra la plataforma de TokenCash si se ha canjeado el cupón.

El proceso continúa y resta del total, el saldo descontado del cupón.

![alt text](https://diurvanconsultores.com/wp-content/uploads/2020/05/Naturagel-TokenCash5-754x1024.jpg?raw=true)
![alt text](https://diurvanconsultores.com/wp-content/uploads/2020/05/Naturagel-TokenCash2.jpg?raw=true)

https://diurvanconsultores.com/portafolio/plugin-integracion-tokencash/